import sys
print('>'*10, 'Fibonacci')
def fibonacci(n): #generator function        
    a, b, counter = 0, 1, 0
    while True:
        if (counter > n):
            
            n = int(input('Again ? or press 0 to exit\n'))
            a, b, counter = 0, 1, 0
            if (n==00):
                print("0")
                return
        yield a
        a, b = b, a + b
        counter += 1
    
    
n = int(input('Enter value of \"n\" (00 to exit)\n -->'))
f=fibonacci(n)
while True:
    try:
        print (next(f), end=" ")
    except StopIteration:
        print("Good bye!")
        sys.exit()