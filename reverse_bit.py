import sys
print('>'*10, 'REVERSE BIT')
numbA = int(input('Enter Character (or press the 0 key to exit)\n -->'))
while (numbA>0):
    numbAbin= bin(numbA)
    
    numbAstr=str (numbAbin)

    hold=numbAstr[2:]
    
    leng=len(hold)
    holdB=''
    while (leng > 0) :
            holdB += (hold [leng-1]) 
            leng-= 1
    print("="*10)
    print (numbA, "in binary is ", numbAbin)
    print (hold)
    print (holdB)
    print("="*10)
    numbA = int(input('Enter Character (or press the 0 key to exit)\n -->'))
else:
    print("GoodBYE")
    sys.exit()

